package ru.mephi.institute.students.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mephi.institute.students.entitiy.Student;
import ru.mephi.institute.students.entitiy.StudentsGroup;
import ru.mephi.institute.students.repository.StudentsGroupRepository;

import java.util.*;

@Service
@AllArgsConstructor
public class StudentsGroupService {
    private final StudentsGroupRepository studentsGroupRepository;

    public List<StudentsGroup> getAllGroups(){
        return studentsGroupRepository.findAll();
    }

    public List<Student> getAllStudentsInGroup(UUID id){
        Optional<StudentsGroup> optionalStudentsGroup = studentsGroupRepository.findById(id);
        if(optionalStudentsGroup.isPresent()){
            return new ArrayList<>(optionalStudentsGroup.get().getStudents());
        }
        else
            return new LinkedList<>();
    }
}
