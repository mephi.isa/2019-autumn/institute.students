package ru.mephi.institute.students.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.mephi.institute.students.entitiy.AppRole;
import ru.mephi.institute.students.entitiy.UserRole;
import ru.mephi.institute.students.model.AppUserBody;

@Repository
@Transactional
public class AppRoleDAO {

    @Autowired
    private EntityManager entityManager;

    public List<String> getRoleNames(Long userId) {
        String sql = "Select ur.appRole.roleName from " + UserRole.class.getName() + " ur " //
                + " where ur.appUser.userId = :userId ";
        Query query = entityManager.createQuery(sql, String.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    public AppRole findRoleByName(String role){
        String sql = "FROM " + AppRole.class.getName() + " ar WHERE ar.roleName=:role";
        Query query = entityManager.createQuery(sql, AppRole.class);
        query.setParameter("role", role);
        return (AppRole) query.getSingleResult();
    }
}