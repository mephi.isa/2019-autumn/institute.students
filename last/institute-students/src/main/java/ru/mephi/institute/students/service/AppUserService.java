package ru.mephi.institute.students.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mephi.institute.students.entitiy.AppRole;
import ru.mephi.institute.students.entitiy.AppUser;
import ru.mephi.institute.students.entitiy.UserRole;
import ru.mephi.institute.students.model.AppUserBody;
import ru.mephi.institute.students.repository.AppRoleDAO;
import ru.mephi.institute.students.repository.AppUserDAO;
import ru.mephi.institute.students.repository.UserRoleDAO;
import ru.mephi.institute.students.utils.EncryptedPasswordUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class AppUserService {

    @Autowired
    private final AppUserDAO appUserDAO;

    @Autowired
    private final UserRoleDAO userRoleDAO;

    @Autowired
    private final AppRoleDAO appRoleDAO;

    @Transactional
    public void createAppUser(AppUserBody appUserBody) {
        System.out.println(appUserBody.getEncrytedPassword());
        AppUser appUser = new AppUser(appUserBody.getUserName(), EncryptedPasswordUtils.encryptePassword(appUserBody.getEncrytedPassword()));
        appUser.setEnabled(true);
        appUserDAO.save(appUser);
        Optional<AppUser> appUserFromDB = appUserDAO.findByUserName(appUser.getUserName());
        AppRole appRole = appRoleDAO.findRoleByName(appUserBody.getRole());
        UserRole userRole = new UserRole(appUserFromDB.get(), appRole);
        userRoleDAO.save(userRole);
    }

    public List<AppUserBody> getAllUsers() {
        List<UserRole> userRoles = userRoleDAO.findAll();
        return userRoles.stream().filter(userRole -> !userRole.getAppRole().getRoleName().equals("ROLE_ADMIN") &&
                !userRole.getAppRole().getRoleName().equals("ROLE_USER"))
                .map(userRole -> {
                            AppUserBody appUserBody = new AppUserBody();
                            appUserBody.setUserName(userRole.getAppUser().getUserName());
                            appUserBody.setRole(userRole.getAppRole().getRoleName());
                            appUserBody.setId(userRole.getAppUser().getUserId());
                            return appUserBody;
                        }
                ).collect(Collectors.toList());
    }

    public AppUserBody getAppUserBodyById(Long id) {
        Optional<AppUser> optionalAppUser = appUserDAO.findById(id);
        if (optionalAppUser.isPresent()) {
            AppUser appUser = optionalAppUser.get();
            List<UserRole> userRoles = userRoleDAO.findAll();
            Optional<UserRole> userRoleOptional = userRoles.stream()
                    .filter(userRole -> userRole.getAppUser().getUserId().equals(appUser.getUserId()))
                    .findFirst();
            if (userRoleOptional.isPresent()) {
                AppUserBody appUserBody = new AppUserBody();
                appUserBody.setId(id);
                appUserBody.setUserName(appUser.getUserName());
                appUserBody.setEncrytedPassword("");
                appUserBody.setRole(userRoleOptional.get().getAppRole().getRoleName());
                return appUserBody;
            }
        }
        return null;
    }

    public void updateAppUser(AppUserBody appUserBody) {
        Optional<AppUser> appUserOptional = appUserDAO.findById(appUserBody.getId());
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();
            appUser.setEncrytedPassword(EncryptedPasswordUtils.encryptePassword(appUserBody.getEncrytedPassword()));
            appUser.setUserName(appUserBody.getUserName());
            appUserDAO.save(appUser);
            List<UserRole> userRoles = userRoleDAO.findAll();
            Optional<UserRole> optionalUserRole = userRoles.stream().filter(userRole ->
                    userRole.getAppUser().getUserId().equals(appUser.getUserId())).findFirst();
            UserRole userRole = optionalUserRole.get();
            userRole.setAppRole(appRoleDAO.findRoleByName(appUserBody.getRole()));
            userRoleDAO.save(userRole);
        }
    }

    public void deleteUserById(Long id){
        Optional<AppUser> appUserOptional = appUserDAO.findById(id);
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();
            Optional<UserRole> userRoleOptional = userRoleDAO.findByAppUser(appUser);
            if(userRoleOptional.isPresent()){
                UserRole userRole = userRoleOptional.get();
                userRoleDAO.delete(userRole);
                appUserDAO.delete(appUser);
            }
        }
    }

}
