package ru.mephi.institute.students.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "accountability")
public class AccountabilityProperties {
    private String login;
    private String password;
    private String host;
    private String loginUrl;
    private String exclusionUrl;
    private String transferUrl;
    private String distributionUrl;
}
