package ru.mephi.institute.students.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mephi.institute.students.entitiy.AppUser;
import ru.mephi.institute.students.entitiy.UserRole;

import java.util.Optional;

public interface UserRoleDAO extends JpaRepository<UserRole, Long> {
    Optional<UserRole> findByAppUser(AppUser appUser);
}

