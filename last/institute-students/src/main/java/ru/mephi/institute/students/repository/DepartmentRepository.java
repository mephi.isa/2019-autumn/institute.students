package ru.mephi.institute.students.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mephi.institute.students.entitiy.Department;

import java.util.Optional;
import java.util.UUID;

public interface DepartmentRepository extends JpaRepository<Department, UUID> {
    Optional<Department> findByName(String name);
}
