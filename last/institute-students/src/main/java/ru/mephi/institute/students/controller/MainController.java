package ru.mephi.institute.students.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.mephi.institute.students.entitiy.Student;
import ru.mephi.institute.students.entitiy.StudentsGroup;
import ru.mephi.institute.students.model.AppUserBody;
import ru.mephi.institute.students.model.EditStudentBody;
import ru.mephi.institute.students.model.Role;
import ru.mephi.institute.students.service.*;
import ru.mephi.institute.students.utils.WebUtils;

@Controller
@AllArgsConstructor
public class MainController {

    private final AppUserService appUserService;
    private final StudentsGroupService studentsGroupService;
    private final StudentService studentService;
    private ListingStudents listingStudents;
    private AutomaticTransfer automaticTransfer;
    private ExclusionStudents exclusionStudents;

    @RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
    public String welcomePage(Model model) {
        model.addAttribute("title", "Welcome");
        model.addAttribute("message", "This is welcome page!");
        return "welcomePage";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(Model model, Principal principal) {

        User loginedUser = (User) ((Authentication) principal).getPrincipal();

        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);

        return "adminPage";
    }

    @GetMapping(value = "/administration")
    public String administrationPage(Model model, Principal principal){
        User loginedUser = (User) ((Authentication) principal).getPrincipal();

        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);

        return "administrationPage";
    }

    @GetMapping(value = "/dean")
    public String deanPage(Model model, Principal principal){
        User loginedUser = (User) ((Authentication) principal).getPrincipal();

        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);

        return "deanPage";
    }

    @GetMapping(value = "/department")
    public String departmentPage(Model model, Principal principal){
        User loginedUser = (User) ((Authentication) principal).getPrincipal();

        String userInfo = WebUtils.toString(loginedUser);
        model.addAttribute("userInfo", userInfo);

        return "departmentPage";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(Model model) {
        return "loginPage";
    }

    @RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
    public String logoutSuccessfulPage(Model model) {
        model.addAttribute("title", "Logout");
        return "logoutSuccessfulPage";
    }

    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public String userInfo(Model model, Principal principal) {
        // After user login successfully.
        String userName = principal.getName();

        System.out.println("User Name: " + userName);

        User loginedUser = (User) ((Authentication) principal).getPrincipal();
        if(loginedUser.getAuthorities().stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority()
                .equals(Role.ADMIN.toString())))
            return "redirect:/admin";
        else if(loginedUser.getAuthorities().stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority()
                .equals(Role.DEAN.toString())))
            return "redirect:/dean";
        else if(loginedUser.getAuthorities().stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority()
                .equals(Role.DEPARTMENT.toString())))
            return "redirect:/department";
        else if(loginedUser.getAuthorities().stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority()
                .equals(Role.ADMINISTRATION.toString())))
            return "redirect:/administration";
        else {
            String userInfo = WebUtils.toString(loginedUser);
            model.addAttribute("userInfo", userInfo);
            return "userInfoPage";
        }
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accessDenied(Model model, Principal principal) {

        if (principal != null) {
            User loginedUser = (User) ((Authentication) principal).getPrincipal();

            String userInfo = WebUtils.toString(loginedUser);

            model.addAttribute("userInfo", userInfo);

            String message = "Hi " + principal.getName() //
                    + "<br> You do not have permission to access this page!";
            model.addAttribute("message", message);

        }

        return "403Page";
    }

    @RequestMapping(value = "/addUser")
    public String addUser(Model model){
        model.addAttribute("newUser", new AppUserBody());
        return "add-user";
    }

    @PostMapping(value = "/createUser")
    public String createUser(AppUserBody appUser){
        appUserService.createAppUser(appUser);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/listUsers")
    public String getAllUsers(Model model)
    {
        List<AppUserBody> list = appUserService.getAllUsers();
        model.addAttribute("users", list);
        return "list-users";
    }

    @RequestMapping(value = "/listGroups")
    public String listGroups(Model model)
    {
        List<StudentsGroup> list = studentsGroupService.getAllGroups();
        model.addAttribute("groups", list);
        return "choose-group";
    }

    @RequestMapping(value = "/listStudentsGroup/{id}")
    public String listStudents(Model model, @PathVariable("id") UUID id){
        List<Student> studentSet = studentsGroupService.getAllStudentsInGroup(id);
        model.addAttribute("students", studentSet);
        return "list-students";
    }

    @RequestMapping(value = "/editUser/{id}")
    public String editUser(Model model, @PathVariable("id") Long id){
        AppUserBody appUserBody = appUserService.getAppUserBodyById(id);
        model.addAttribute("userBody", appUserBody);
        return "edit-user";
    }

    @RequestMapping(value = "/editStudent/{id}")
    public String editStudent(Model model, @PathVariable("id") UUID id) {
        model.addAttribute("student", studentService.getStudentById(id));
        return "edit-student";
    }

    @RequestMapping(value = "/updateStudent")
    public String updateStudent(EditStudentBody student, Principal principal){
        studentService.updateStudent(student);

        User loginedUser = (User) ((Authentication) principal).getPrincipal();

        if(loginedUser.getAuthorities().stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority()
                .equals(Role.DEAN.toString())))
            return "redirect:/dean";
        else if(loginedUser.getAuthorities().stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority()
                .equals(Role.DEPARTMENT.toString())))
            return "redirect:/department";
        else if(loginedUser.getAuthorities().stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority()
                .equals(Role.ADMINISTRATION.toString())))
            return "redirect:/administration";
        else
            return "redirect:/";
    }

    @RequestMapping(value = "/updateUser")
    public String updateUser(AppUserBody appUser){
        appUserService.updateAppUser(appUser);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/deleteUser/{id}")
    public String deleteUser(Model model, @PathVariable("id") Long id) {
        appUserService.deleteUserById(id);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/deleteStudent/{id}")
    public String archiveStudent(Model model, @PathVariable("id") UUID id){
        studentService.archiveStudentById(id);
        return "redirect:/administration";
    }

    @RequestMapping(value = "/externalTransfer")
    public String externalTransfer(Model model){
        automaticTransfer.transfer();
        return "redirect:/administration";
    }

    @RequestMapping(value = "/externalDistribution")
    public String externalDistribution(Model model) throws IOException {
        listingStudents.distribution();
        return "redirect:/administration";
    }

    @RequestMapping(value = "/externalExclusion")
    public String externalExclusion(Model model) throws IOException{
        exclusionStudents.exclusion();
        return "redirect:/administration";
    }
}