package ru.mephi.institute.students.model;

public enum Role {
    ADMIN("ROLE_ADMIN"), //админ системы
    USER("ROLE_USER"),
    DEPARTMENT("ROLE_DEPARTMENT"), //пользователь кафедры
    DEAN("ROLE_DEAN"), //пользователь деканата
    ADMINISTRATION("ROLE_ADMINISTRATION"); //пользователь ректората

    private String role;
    Role(String role){
        this.role = role;
    }

    @Override
    public String toString(){
        return role;
    }
}
