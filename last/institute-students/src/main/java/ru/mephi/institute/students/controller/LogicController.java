package ru.mephi.institute.students.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.mephi.institute.students.service.AutomaticTransfer;
import ru.mephi.institute.students.service.ExclusionStudents;
import ru.mephi.institute.students.service.ListingStudents;

import java.io.IOException;

@RestController
@AllArgsConstructor
public class LogicController {

    private ListingStudents listingStudents;
    private AutomaticTransfer automaticTransfer;
    private ExclusionStudents exclusionStudents;

    @PostMapping(value = "/distribution/external")
    public ResponseEntity listingStudents() throws IOException {
        return ResponseEntity.ok(listingStudents.distribution());
    }

    @PostMapping(value = "/transfer")
    @ResponseStatus(HttpStatus.OK)
    public void authomaticTransfer(){
        automaticTransfer.transfer();
    }

    @PostMapping(value = "/exclusion/external")
    public ResponseEntity authomaticExclusion() throws IOException {
        return ResponseEntity.ok(exclusionStudents.exclusion());
    }


}
