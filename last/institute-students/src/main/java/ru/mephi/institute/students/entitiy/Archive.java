package ru.mephi.institute.students.entitiy;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import ru.mephi.institute.students.common.TimestampDeserializer;
import ru.mephi.institute.students.common.TimestampSerializer;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;


@Data
@Entity(name = "Archive")
@Table(name = "archive")
public class Archive {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;

    @JsonDeserialize(using = TimestampDeserializer.class)
    @JsonSerialize(using = TimestampSerializer.class)
    @Column(name = "birth_date")
    private Timestamp birthDate;
    @Column(name = "identity_document_number")
    private String identityDocumentNumber;
    @Column(name = "semester")
    private int semester;
    @Column(name = "degree")
    private String degree;
    @Column(name = "graduate")
    private Boolean graduate;
    @Column(name = "archived_date")
    private Timestamp archivedDate;

    public Archive(String name, String surname, Timestamp birthDate, String identityDocumentNumber,
                   int semester, String degree, Boolean graduate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.identityDocumentNumber = identityDocumentNumber;
        this.semester = semester;
        this.archivedDate = Timestamp.valueOf(LocalDateTime.now());
        this.degree = degree;
        this.graduate = graduate;
    }
}
