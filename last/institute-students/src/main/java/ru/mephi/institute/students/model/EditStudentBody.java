package ru.mephi.institute.students.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mephi.institute.students.entitiy.StudentsGroup;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
public class EditStudentBody {
    private UUID id;
    private String name;
    private String surname;
    private Timestamp birthDate;
    private String identityDocumentNumber;
    private UUID studentsGroup;
    private List<StudentsGroup> groups;
}
