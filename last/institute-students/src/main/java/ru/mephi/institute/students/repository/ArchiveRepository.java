package ru.mephi.institute.students.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mephi.institute.students.entitiy.Archive;

import java.util.UUID;

public interface ArchiveRepository extends JpaRepository<Archive, UUID> {
}
