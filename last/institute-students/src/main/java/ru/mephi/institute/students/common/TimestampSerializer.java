package ru.mephi.institute.students.common;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

public class TimestampSerializer extends StdSerializer<Timestamp> {

    private static final long serialVersionUID = 1L;

    public TimestampSerializer(){
        super(Timestamp.class);
    }

    @Override
    public void serialize(Timestamp value, JsonGenerator gen, SerializerProvider sp) throws IOException {
        gen.writeString(value.toLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE));
    }
}