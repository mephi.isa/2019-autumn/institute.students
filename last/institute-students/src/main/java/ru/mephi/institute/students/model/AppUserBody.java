package ru.mephi.institute.students.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AppUserBody {

    private String userName;
    private String encrytedPassword;
    private String role;
    private Long id;
}
