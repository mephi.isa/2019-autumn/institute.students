package ru.mephi.institute.students.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mephi.institute.students.entitiy.Student;

import java.util.Optional;
import java.util.UUID;

public interface StudentRepository extends JpaRepository<Student, UUID> {
    Optional<Student> findByIdentityDocumentNumber(String identityDocumentNumber);
}
