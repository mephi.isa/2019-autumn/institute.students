package ru.mephi.institute.students.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mephi.institute.students.entitiy.Archive;
import ru.mephi.institute.students.entitiy.Student;
import ru.mephi.institute.students.entitiy.StudentsGroup;
import ru.mephi.institute.students.repository.ArchiveRepository;
import ru.mephi.institute.students.repository.StudentRepository;
import ru.mephi.institute.students.repository.StudentsGroupRepository;
import ru.mephi.institute.students.service.client.AccountabilityClient;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ExclusionStudents {
    private ArchiveRepository archiveRepository;
    private StudentsGroupRepository studentsGroupRepository;
    private StudentRepository studentRepository;
    private AccountabilityClient accountabilityClient;

    public List<Student> exclusion() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<StudentsGroup> groups = Arrays.asList(objectMapper.readValue(accountabilityClient.getStudentsForExclusion(), StudentsGroup[].class));
        List<Student> excludedStudents = new LinkedList<>();
        for(StudentsGroup group: groups){
            Optional<StudentsGroup> studentsGroupEntityRow = studentsGroupRepository.findByName(group.getName());
            if(studentsGroupEntityRow.isPresent()){
                for(Student student: group.getStudents()){
                    Optional<Student> optionalStudentEntity = studentRepository
                            .findByIdentityDocumentNumber(student.getIdentityDocumentNumber());
                    if(optionalStudentEntity.isPresent()){
                        excludedStudents.add(student);
                        studentRepository.deleteById(optionalStudentEntity.get().getId());
                        archiveRepository.save(new Archive(student.getName(), student.getSurname(), student.getBirthDate(),
                                student.getIdentityDocumentNumber(), studentsGroupEntityRow.get().getSemester(),
                                studentsGroupEntityRow.get().getDegree(), Boolean.FALSE));
                    }
                }
            }
        }
        return excludedStudents;
    }
}
