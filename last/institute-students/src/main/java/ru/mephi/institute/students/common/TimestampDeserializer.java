package ru.mephi.institute.students.common;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;

public class TimestampDeserializer extends StdDeserializer<Timestamp> {

    private static final long serialVersionUID = 1L;

    protected TimestampDeserializer() {
        super(Timestamp.class);
    }


    @Override
    public Timestamp deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
        return Timestamp.valueOf(LocalDate.parse(jp.readValueAs(String.class)).atStartOfDay());
    }

}