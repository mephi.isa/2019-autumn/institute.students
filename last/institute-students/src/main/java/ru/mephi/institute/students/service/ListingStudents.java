package ru.mephi.institute.students.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mephi.institute.students.entitiy.Department;
import ru.mephi.institute.students.entitiy.Student;
import ru.mephi.institute.students.entitiy.StudentsGroup;
import ru.mephi.institute.students.repository.DepartmentRepository;
import ru.mephi.institute.students.repository.StudentRepository;
import ru.mephi.institute.students.repository.StudentsGroupRepository;
import ru.mephi.institute.students.service.client.AccountabilityClient;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ListingStudents {
    private DepartmentRepository departmentRepository;
    private StudentsGroupRepository studentsGroupRepository;
    private StudentRepository studentRepository;
    private AccountabilityClient accountabilityClient;

    public String distribution() throws IOException {
        String studentsForDistribution = accountabilityClient.getStudentsForDistribution();
        if(studentsForDistribution != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            List<Department> departments = Arrays.asList(objectMapper.readValue(studentsForDistribution, Department[].class));
            //Сохраняем кафедры
            for (Department department : departments) {
                Optional<Department> departmentOptional = departmentRepository.findByName(department.getName());
                if (!departmentOptional.isPresent()) {
                    Department department1 = new Department();
                    department1.setName(department.getName());
                    departmentRepository.save(department1);
                }
            }
            Set<StudentsGroup> studentsGroups = departments.stream().map(Department::getStudentsGroups)
                    .flatMap(Set::stream).collect(Collectors.toSet());

            //Сохраняем группы
            for (Department department : departments) {
                Department departmentEntity = departmentRepository.findByName(department.getName()).get();
                for (StudentsGroup studentsGroup : department.getStudentsGroups()) {
                    Optional<StudentsGroup> optionalStudentsGroup = studentsGroupRepository.findByName(studentsGroup.getName());
                    if (!optionalStudentsGroup.isPresent()) {
                        studentsGroup.setDepartment(departmentEntity);
                        StudentsGroup studentsGroupForSave = new StudentsGroup(studentsGroup.getName(),
                                studentsGroup.getSemester(), studentsGroup.getDegree());
                        studentsGroupForSave.setDepartment(departmentEntity);
                        studentsGroupRepository.save(studentsGroupForSave);
                    }
                }
            }

            //Сохраняем студентов
            for (StudentsGroup studentsGroup : studentsGroups) {
                StudentsGroup studentsGroupEntity = studentsGroupRepository.findByName(studentsGroup.getName()).get();
                for (Student student : studentsGroup.getStudents()) {
                    Optional<Student> optionalStudentEntityRow = studentRepository
                            .findByIdentityDocumentNumber(student.getIdentityDocumentNumber());
                    if (!optionalStudentEntityRow.isPresent()) {
                        Student studentForSave = new Student(student.getName(), student.getSurname(), student.getBirthDate(),
                                student.getIdentityDocumentNumber());
                        studentForSave.setStudentsGroup(studentsGroupEntity);
                        studentRepository.save(studentForSave);
                    }
                }
            }

            return "Complete";
        }
        else
            return "Students list for distribution is empty";
    }
}
