package ru.mephi.institute.students.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mephi.institute.students.entitiy.Archive;
import ru.mephi.institute.students.entitiy.Student;
import ru.mephi.institute.students.entitiy.StudentsGroup;
import ru.mephi.institute.students.model.Degree;
import ru.mephi.institute.students.repository.ArchiveRepository;
import ru.mephi.institute.students.repository.StudentRepository;
import ru.mephi.institute.students.repository.StudentsGroupRepository;
import ru.mephi.institute.students.service.client.AccountabilityClient;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class AutomaticTransfer {
    private StudentRepository studentRepository;
    private StudentsGroupRepository studentsGroupRepository;
    private AccountabilityClient accountabilityClient;
    private ArchiveRepository archiveRepository;

    public void transfer(){
        List<Student> studentList = studentRepository.findAll();
        Set<StudentsGroup> transferred = new HashSet<>();
        Set<StudentsGroup> graduated = new HashSet<>();
        for (Student student: studentList){
            boolean transferResponse = accountabilityClient.transferOrNot(student);
            int groupSemester = student.getStudentsGroup().getSemester();
            String degree = student.getStudentsGroup().getDegree();
            if(transferResponse && Degree.degreeMap.get(degree) > groupSemester){
                transferred.add(student.getStudentsGroup());
            }
            else if(transferResponse && Degree.degreeMap.get(degree) == groupSemester){
                graduated.add(student.getStudentsGroup());
                studentRepository.deleteById(student.getId());
                archiveRepository.save(new Archive(student.getName(), student.getSurname(), student.getBirthDate(),
                        student.getIdentityDocumentNumber(), student.getStudentsGroup().getSemester(),
                        student.getStudentsGroup().getDegree(), Boolean.TRUE));
            }
            else {
                studentRepository.deleteById(student.getId());
                archiveRepository.save(new Archive(student.getName(), student.getSurname(), student.getBirthDate(),
                        student.getIdentityDocumentNumber(), student.getStudentsGroup().getSemester(),
                        student.getStudentsGroup().getDegree(), Boolean.FALSE));
            }
        }
        for(StudentsGroup transfer: transferred){
            transfer.setSemester(transfer.getSemester() + 1);
            studentsGroupRepository.save(transfer);
        }

        for (StudentsGroup graduate: graduated){
            studentsGroupRepository.deleteById(graduate.getId());
        }
    }

}
