package ru.mephi.institute.students.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
public class StudentsGroupBody {
    private String groupName;
    private UUID id;
}
