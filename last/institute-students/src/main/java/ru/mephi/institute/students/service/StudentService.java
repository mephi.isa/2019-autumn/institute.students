package ru.mephi.institute.students.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mephi.institute.students.entitiy.Archive;
import ru.mephi.institute.students.entitiy.Student;
import ru.mephi.institute.students.entitiy.StudentsGroup;
import ru.mephi.institute.students.model.EditStudentBody;
import ru.mephi.institute.students.repository.ArchiveRepository;
import ru.mephi.institute.students.repository.StudentRepository;
import ru.mephi.institute.students.repository.StudentsGroupRepository;

import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;
    private final StudentsGroupRepository studentsGroupRepository;
    private final ArchiveRepository archiveRepository;

    public EditStudentBody getStudentById(UUID uuid){
        Student student = studentRepository.findById(uuid).orElse(null);
        EditStudentBody editStudentBody = new EditStudentBody();
        if(student != null){
            editStudentBody.setBirthDate(student.getBirthDate());
            editStudentBody.setId(student.getId());
            editStudentBody.setGroups(studentsGroupRepository.findAll());
            editStudentBody.setIdentityDocumentNumber(student.getIdentityDocumentNumber());
            editStudentBody.setName(student.getName());
            editStudentBody.setSurname(student.getSurname());
        }
        return editStudentBody;
    }

    public void updateStudent(EditStudentBody studentBody){
        Optional<Student> studentOptional = studentRepository.findById(studentBody.getId());
        Optional<StudentsGroup> studentsGroupOptional = studentsGroupRepository.findById(studentBody.getStudentsGroup());
        if(studentOptional.isPresent() && studentsGroupOptional.isPresent()) {
            Student student = studentOptional.get();
            student.setName(studentBody.getName());
            student.setSurname(studentBody.getSurname());
            student.setIdentityDocumentNumber(studentBody.getIdentityDocumentNumber());
            student.setStudentsGroup(studentsGroupOptional.get());
            studentRepository.save(student);
        }
    }

    @Transactional
    public void archiveStudentById(UUID id){
        Optional<Student> studentOptional = studentRepository.findById(id);
        if(studentOptional.isPresent()){
            Student student = studentOptional.get();
            studentRepository.delete(student);
            archiveRepository.save(new Archive(student.getName(), student.getSurname(), student.getBirthDate(),
                    student.getIdentityDocumentNumber(), student.getStudentsGroup().getSemester(),
                    student.getStudentsGroup().getDegree(), false));
        }
    }
}
