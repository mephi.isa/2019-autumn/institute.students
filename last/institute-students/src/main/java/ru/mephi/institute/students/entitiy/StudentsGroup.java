package ru.mephi.institute.students.entitiy;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "students_group")
public class StudentsGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    @Getter
    private UUID id;
    @Column(name = "name")
    private String name;
    @Column(name = "semester")
    private int semester;
    @Column(name = "degree")
    private String degree;
    @OneToMany(mappedBy = "studentsGroup", cascade = CascadeType.ALL)
    private Set<Student> students;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "department_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Department department;

    protected StudentsGroup(){}

    public StudentsGroup(String name, int semester, String degree) {
        this.name = name;
        this.semester = semester;
        this.degree = degree;
    }

}
