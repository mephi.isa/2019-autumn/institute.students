package ru.mephi.institute.students.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mephi.institute.students.entitiy.AppUser;

import java.util.Optional;

public interface AppUserDAO extends JpaRepository<AppUser, Long> {
    Optional<AppUser> findByUserName(String username);
}
