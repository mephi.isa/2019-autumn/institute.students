package ru.mephi.institute.students.model;

import java.util.HashMap;
import java.util.Map;

public enum Degree {
    BACHELOOR("BACHELOOR"),
    MAGISTER("MAGISTER"),
    ASPIRANTURE("ASPIRANTURE");

    private String degree;
    public static final Map<String, Integer> degreeMap = new HashMap<String, Integer>(){{
        put("BACHELOOR", 8);
        put("MAGISTER", 4);
        put("ASPIRANTURE", 6);
    }};

    Degree(String degree){
        this.degree = degree;
    }

    @Override
    public String toString(){
        return degree;
    }
}
