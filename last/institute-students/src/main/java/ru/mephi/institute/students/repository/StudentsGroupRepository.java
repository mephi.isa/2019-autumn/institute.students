package ru.mephi.institute.students.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mephi.institute.students.entitiy.StudentsGroup;

import java.util.Optional;
import java.util.UUID;

public interface StudentsGroupRepository extends JpaRepository<StudentsGroup, UUID> {
    Optional<StudentsGroup> findByName(String name);
}
