package ru.mephi.institute.students.service.client;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.mephi.institute.students.entitiy.Student;
import ru.mephi.institute.students.properties.AccountabilityProperties;

public class AccountabilityClient {

    private final RestTemplate restTemplate;
    private final AccountabilityProperties accountabilityProperties;
    private final String authDetails;

    public AccountabilityClient(RestTemplate restTemplate, AccountabilityProperties accountabilityProperties){
        this.restTemplate = restTemplate;
        this.accountabilityProperties = accountabilityProperties;
        JSONObject authJsonObject = new JSONObject();
        authJsonObject.put("login", accountabilityProperties.getLogin());
        authJsonObject.put("password", accountabilityProperties.getPassword());
        this.authDetails = authJsonObject.toString();

    }

    public boolean transferOrNot(Student student){
        String token = getToken();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Cookie", token);
        HttpEntity<String> request = new HttpEntity<>(null, headers);
        try {
            HttpEntity<String> response = restTemplate.exchange(String.format("%s/%s/%s/%s",
                    accountabilityProperties.getTransferUrl(), student.getStudentsGroup().getName(),
                    student.getName(), student.getSurname()), HttpMethod.GET, request, String.class);
            return Boolean.valueOf(response.getBody());
        }
        catch (HttpClientErrorException ex){
            return false;
        }
    }

    public String getStudentsForExclusion(){
        String token = getToken();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Cookie", token);
        HttpEntity<String> request = new HttpEntity<>(null, headers);
        HttpEntity<String> response = restTemplate.exchange(accountabilityProperties.getExclusionUrl(),
                HttpMethod.GET, request, String.class);
        return response.getBody();
    }

    public String getStudentsForDistribution(){
        String token = getToken();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Cookie", token);
        HttpEntity<String> request = new HttpEntity<>(null, headers);
        HttpEntity<String> response = restTemplate.exchange(accountabilityProperties.getDistributionUrl(),
                HttpMethod.GET, request, String.class);
        return response.getBody();
    }

    private String getToken(){
        HttpEntity<String> request = new HttpEntity<>(authDetails);
        HttpEntity<String> response = restTemplate.exchange(accountabilityProperties.getLoginUrl(), HttpMethod.POST,
                request, String.class);
        HttpHeaders headers = response.getHeaders();
        return headers.getFirst(HttpHeaders.SET_COOKIE);
    }

}
