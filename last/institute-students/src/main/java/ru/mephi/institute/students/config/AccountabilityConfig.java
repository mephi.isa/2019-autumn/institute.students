package ru.mephi.institute.students.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import ru.mephi.institute.students.properties.AccountabilityProperties;
import ru.mephi.institute.students.service.client.AccountabilityClient;

@Configuration
@EnableConfigurationProperties(AccountabilityProperties.class)
public class AccountabilityConfig {

    @Bean
    public AccountabilityClient accountabilityClient(AccountabilityProperties accountabilityProperties){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(accountabilityProperties.getHost()));
        return new AccountabilityClient(restTemplate, accountabilityProperties);
    }
}
