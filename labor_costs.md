| N  |Задача                               |             Планируемые трудозатраты                       |         Фактические трудозатраты         
|----|-------------------------------------|------------------------------------------------------------|------------------------------------------
|  1 |Реализация наборов бизнес объектов   |8 ЧЧ  (человеко-часа)                                       |  6 ЧЧ                                 
|  2 |Написание юнит-тестов                |6 ЧЧ                                                        |  6 ЧЧ
|  3 |Реализация прикладной логики         |20 ЧЧ                                                       |
|  4 |Написание интеграционных тестов      |12 ЧЧ                                                        
