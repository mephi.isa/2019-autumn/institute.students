import datetime


class GenericError(Exception):

    def __init__(self, message):
        super().__init__(message)


class Student:

    def __init__(self, name: str, surname: str, birth_date: datetime.datetime, identity_document_number: str, id: str, semester: int):
        self.name: str = name
        self.surname: str = surname
        self.id: str = id
        self.birth_date: datetime = birth_date
        self.identity_document_number: str = identity_document_number
        self.semester: int = semester

    def set_id(self, id: str):
        self.id = id

    def get_id(self) -> str:
        return self.id

    def set_birth_date(self, birth_date: datetime):
        self.birth_date = birth_date

    def get_birth_date(self) -> datetime:
        return self.birth_date

    def set_identity_document_number(self, identity_document_number):
        self.identity_document_number = identity_document_number

    def get_identity_document_number(self):
        return self.identity_document_number

    def set_name(self, name: str):
        self.name = name

    def get_name(self) -> str:
        return self.name

    def set_surname(self, surname: str):
        self.surname = surname

    def get_surname(self) -> str:
        return self.surname

    def set_semester(self, semester: int):
        self.semester = semester

    def get_semester(self) -> int:
        return self.semester


class StudentsGroup:
    def __init__(self, name: str, id: str, semester: int):
        self.id: str = id
        self.name: str = name
        self.semester: int = semester
        self.students: list = []

    def get_students(self) -> list:
        return self.students

    def add_student(self, student: Student):
        for k, v in enumerate(self.students):
            if v.get_id() == student.get_id() or v.get_identity_document_number() == student.get_identity_document_number():
                raise GenericError("Student with given id or identity document number already exists in this group")
        if student.semester == self.semester:
            self.students.append(student)
        else:
            raise GenericError("Error. Student cannot be added to this group.")

    def exclude_student(self, student: Student):
        self.students.remove(student)

    def set_id(self, id: str):
        self.id = id

    def get_id(self) -> str:
        return self.id

    def set_name(self, name: str):
        self.name = name

    def get_name(self) -> str:
        return self.name

    def set_semester(self, semester: int):
        self.semester = semester

    def get_semester(self) -> int:
        return self.semester

    def find_student(self, **kwargs):
        out_student: Student = None
        if "student_id" in kwargs:
            for i, student in enumerate(self.students):
                if student.get_id() == student_id:
                    out_student = student
                    break
        elif "identity_document_number" in kwargs:
            for i, student in enumerate(self.students):
                if student.get_identity_document_number() == kwargs["identity_document_number"]:
                    out_student = student
                    break
        return out_student


class Department:
    def __init__(self, name, id):
        self.name = name
        self.id = id
        self.groups: list = []

    def set_id(self, id: str):
        self.id = id

    def get_id(self) -> str:
        return self.id

    def set_name(self, name: str):
        self.name = name

    def get_name(self) -> str:
        return self.name

    def add_group(self, group: StudentsGroup):
        for k, v in enumerate(self.groups):
            if v.get_id() == group.get_id() or v.get_name() == group.get_name():
                raise GenericError("Group with given id or name already exists in this department")
        self.groups.append(group)

    def remove_group(self, group: StudentsGroup):
        self.groups.remove(group)

    def get_groups(self) -> list:
        return self.groups

    def find_group(self, group_id: str) -> StudentsGroup:
        out_group: StudentsGroup = None
        for i, group in enumerate(self.groups):
            if group.id == group_id:
                out_group = group
        return out_group


class StudentHistory(Student):

    def __init__(self, student: Student, date_archived):
        super().__init__(
            name=student.name,
            surname=student.surname,
            id=student.id,
            birth_date=student.birth_date,
            identity_document_number=student.identity_document_number,
            semester=student.semester
        )
        self.date_archived: datetime = date_archived

    def set_date_archived(self, date_archived: datetime):
        self.date_archived = date_archived

    def get_date_archived(self) -> datetime:
        return self.date_archived


class Archive:

    def __init__(self):
        self.students_history: list = []

    def add_student(self, student: Student):
        self.students_history.append(StudentHistory(student=student, date_archived=datetime.datetime.now().replace(hour=0,minute=0,second=0,microsecond=0)))

    def find_archived_student(self, **kwargs) -> StudentHistory:
        out_student: StudentHistory = None
        if "student_id" in kwargs and "date_archived" in kwargs:
            for i, student in enumerate(self.students_history):
                if student.get_id() == kwargs["student_id"] and student.get_date_archived() == kwargs["date_archived"]:
                    out_student = student
                    break
        elif "identity_document_number" in kwargs and "date_archived" in kwargs:
            for i, student in enumerate(self.students_history):
                if student.get_identity_document_number() == kwargs["identity_document_number"] and student.date_archived == kwargs["date_archived"]:
                    out_student = student
                    break
        elif "student_id" in kwargs:
            for i, student in enumerate(self.students_history):
                if student.get_id() == kwargs["student_id"]:
                    if out_student == None:
                        out_student = student
                    elif out_student.get_date_archived() < student.get_date_archived():
                        out_student = student
        elif "identity_document_number" in kwargs:
            for i, student in enumerate(self.students_history):
                if student.get_identity_document_number() == kwargs["identity_document_number"]:
                    if out_student == None:
                        out_student = student
                    elif out_student.get_date_archived() < student.get_date_archived():
                        out_student = student
        else:
            raise GenericError("At least student_id or identity_document_number arguments are required")
        return out_student
