import unittest
import datetime
from models import Department, StudentsGroup, Student, Archive, StudentHistory


class ModelTest(unittest.TestCase):

    def setUp(self):
        self.department = Department(
            name="IIKS",
            id="1",
        )
        self.group = StudentsGroup(
            id="1",
            name="M19-512",
            semester=1
        )
        self.student = Student(
            name="Vasya",
            surname="Petrov",
            birth_date=datetime.datetime(1994, 5, 5),
            identity_document_number="12345",
            id="1",
            semester=1
        )
        self.archive = Archive()
        self.group.add_student(self.student)
        self.department.add_group(self.group)
        self.student_history = StudentHistory(
            student=self.student,
            date_archived=datetime.datetime.now()
        )

    def test_student_id(self):
        self.student.set_id("1")
        self.group.find_student(id=self.student.get_id())

    def test_student_birth_date(self):
        self.student.set_birth_date(datetime.datetime(1998, 10, 10))
        self.group.find_student(birth_date=self.student.get_birth_date())

    def test_student_identity_document_number(self):
        self.student.set_identity_document_number("7867")
        self.group.find_student(identity_document_number=self.student.get_identity_document_number())

    def test_student_name(self):
        self.student.set_name("Dasha")
        self.group.find_student(name=self.student.get_name())

    def test_student_surname(self):
        self.student.set_surname("M")
        self.group.find_student(surname=self.student.get_surname())

    def test_student_semester(self):
        self.student.set_semester(1)
        self.assertEqual(self.student.get_semester(), 1)

    def test_group_name(self):
        self.group.set_name("S-15")
        self.department.find_group(group_id=self.group.get_id())

    def test_group_id(self):
        self.group.set_id("6")
        self.department.find_group(group_id=self.group.get_id())

    def test_group_semester(self):
        self.group.set_semester(1)
        self.assertEqual(self.group.get_semester(), 1)

    def test_group_add_student(self):
        self.group.add_student(Student(
            name="Bobby",
            surname="Robert",
            id="2",
            birth_date=datetime.datetime(1989, 10, 1),
            identity_document_number="123456",
            semester=1
        ))
        self.assertIsNotNone(
            self.group.find_student(identity_document_number=self.student.get_identity_document_number()))

    def test_group_exclude_student(self):
        self.assertIsNotNone(
            self.group.find_student(identity_document_number=self.student.get_identity_document_number()))
        self.group.exclude_student(student=self.student)
        self.assertIsNone(self.group.find_student(identity_document_number=self.student.get_identity_document_number()))

    def test_group_find_student(self):
        self.assertIsInstance(self.group.find_student(identity_document_number="12345"), Student)
        self.assertIsNone(self.group.find_student(identity_document_number="00"))

    def test_department_name(self):
        name = "KIB"
        self.department.set_name(name)
        self.assertEqual(self.department.get_name(), name)

    def test_department_id(self):
        id = "098"
        self.department.set_id(id)
        self.assertEqual(self.department.get_id(), id)

    def test_department_add_group(self):
        self.department.add_group(StudentsGroup(
            name="M19-513",
            id="2",
            semester=1
        ))
        self.assertIsInstance(self.department.find_group("2"), StudentsGroup)

    def test_department_remove_group(self):
        self.department.remove_group(self.group)
        self.assertIsNone(self.department.find_group(self.group.get_id()))

    def test_department_get_groups(self):
        self.assertEqual(len(self.department.get_groups()), 1)

    def test_department_find_group(self):
        self.assertIsNotNone(self.department.find_group(group_id="1"))

    def test_student_history(self):
        date = datetime.datetime.now()
        self.student_history.set_date_archived(date_archived=date)
        self.assertEqual(self.student_history.get_date_archived(), date)

    def test_archive(self):
        self.archive.add_student(self.student)
        self.assertIsInstance(self.archive.find_archived_student(student_id="1"), StudentHistory)
        self.assertIsInstance(self.archive.find_archived_student(student_id="1",
                                                                 date_archived=datetime.datetime.now().replace(hour=0,
                                                                                                               minute=0,
                                                                                                               second=0,
                                                                                                               microsecond=0)),
                              StudentHistory)


if __name__ == '__main__':
    unittest.main()
