package ru.mephi.institute.students.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mephi.institute.students.entitiy.Student;
import ru.mephi.institute.students.repository.StudentRepository;
import ru.mephi.institute.students.service.client.AccountabilityClient;

import java.util.LinkedList;
import java.util.List;

@Service
@AllArgsConstructor
public class AutomaticTransfer {
    private StudentRepository studentRepository;
    private AccountabilityClient accountabilityClient;

    public List<Student> transfer(){
        List<Student> studentList = studentRepository.findAll();
        List<Student> transferred = new LinkedList<>();
        for (Student student: studentList){
            if(accountabilityClient.transferOrNot(student)){
                student.setSemester(student.getSemester() + 1);
                studentRepository.save(student);
                transferred.add(student);
            }
        }
        return transferred;
    }

}
