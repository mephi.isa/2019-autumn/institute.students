package ru.mephi.institute.students.entitiy;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import ru.mephi.institute.students.common.TimestampDeserializer;
import ru.mephi.institute.students.common.TimestampSerializer;


import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Data
@Entity
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @JsonDeserialize(using = TimestampDeserializer.class)
    @JsonSerialize(using = TimestampSerializer.class)
    @Column(name = "birth_date")
    private Timestamp birthDate;
    @Column(name = "identity_document_number")
    private String identityDocumentNumber;
    @Column(name = "semester")
    private int semester;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "students_group_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private StudentsGroup studentsGroup;

    protected Student(){}

    public Student(String name, String surname, Timestamp birthDate, String identityDocumentNumber, int semester) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.identityDocumentNumber = identityDocumentNumber;
        this.semester = semester;
    }

}
