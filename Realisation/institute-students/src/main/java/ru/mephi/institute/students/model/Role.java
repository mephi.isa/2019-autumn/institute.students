package ru.mephi.institute.students.model;

public enum Role {
    ADMIN("admin"), //админ системы
    DEPARTMENT("department"), //пользователь кафедры
    DEAN("dean"), //пользователь деканата
    ADMINISTRATION("administration"); //пользователь ректората

    private String role;
    Role(String role){
        this.role = role;
    }

    @Override
    public String toString(){
        return role;
    }
}
