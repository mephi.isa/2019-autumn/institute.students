package ru.mephi.institute.students.entitiy;

import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@Table(name = "students_group")
public class StudentsGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;
    @Column(name = "name")
    private String name;
    @Column(name = "semester")
    private int semester;
    @OneToMany(mappedBy = "studentsGroup", cascade = CascadeType.ALL)
    private Set<Student> students;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "department_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Department department;

    protected StudentsGroup(){}

    public StudentsGroup(String name, int semester) {
        this.name = name;
        this.semester = semester;
    }

}
