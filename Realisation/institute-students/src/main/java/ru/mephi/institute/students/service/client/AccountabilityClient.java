package ru.mephi.institute.students.service.client;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import ru.mephi.institute.students.entitiy.Student;

@Component
@NoArgsConstructor
public class AccountabilityClient {
    public boolean transferOrNot(Student student){
        return Math.random() < 0.5;
    }

    public String getStudentsForExclusion(){
        return "[\n" +
                "  {\n" +
                "    \"name\": \"M19-512\",\n" +
                "    \"semester\": 1,\n" +
                "    \"students\": [\n" +
                "      {\n" +
                "        \"name\": \"Kate\",\n" +
                "        \"surname\": \"\",\n" +
                "        \"birthDate\": \"1997-03-18\",\n" +
                "        \"identityDocumentNumber\": \"123\",\n" +
                "        \"semester\": 1\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\": \"M18-512\",\n" +
                "    \"semester\": 3,\n" +
                "    \"students\": [\n" +
                "      {\n" +
                "        \"name\": \"Nikolay\",\n" +
                "        \"surname\": \"ASD\",\n" +
                "        \"birthDate\": \"1997-03-18\",\n" +
                "        \"identityDocumentNumber\": \"534534\",\n" +
                "        \"semester\": 3\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "]";
    }

    public String getStudentsForDistribution(){
        return "[{\n" +
                "  \"name\": \"IIKS\",\n" +
                "  \"studentsGroups\": [\n" +
                "    {\n" +
                "      \"name\": \"M19-512\",\n" +
                "      \"semester\": 1,\n" +
                "      \"students\": [\n" +
                "        {\n" +
                "          \"name\": \"Kate\",\n" +
                "          \"surname\": \"\",\n" +
                "          \"birthDate\": \"1997-03-18\",\n" +
                "          \"identityDocumentNumber\": \"123\",\n" +
                "          \"semester\": 1\n" +
                "        },\n" +
                "        {\n" +
                "          \"name\": \"Kirill\",\n" +
                "          \"surname\": \"\",\n" +
                "          \"birthDate\": \"1997-02-18\",\n" +
                "          \"identityDocumentNumber\": \"123123\",\n" +
                "          \"semester\": 1\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"M18-512\",\n" +
                "      \"semester\": 3,\n" +
                "      \"students\": [\n" +
                "        {\n" +
                "          \"name\": \"Nikolay\",\n" +
                "          \"surname\": \"ASD\",\n" +
                "          \"birthDate\": \"1997-03-18\",\n" +
                "          \"identityDocumentNumber\": \"534534\",\n" +
                "          \"semester\": 3\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  ]\n" +
                "}]";
    }
}
